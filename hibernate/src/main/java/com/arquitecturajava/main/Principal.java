package com.arquitecturajava.main;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.arquitecturajava.aplicacion.bo.Libro;

public class Principal {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Session session = null;
		Transaction transaccion = null;
		Principal p = new Principal(); 
		try {
			SessionFactory factoria = new Configuration().configure().
					buildSessionFactory();
			session = factoria.openSession();
			transaccion = session.beginTransaction();
		// se inserta un elemento 	
			Libro libro = new Libro("18", "javadu", "programacion");
			session.saveOrUpdate(libro);			
			
		// se obtiene la lista de elementosw	
			p.mostrarLibros(p.obtenerListaLibros(session));
			
			// se sellecciona un elemento 
			Libro liBorra= p.mostrarSoloLibro(session, "473830");
			
			// se elimina un elemento 
			if(liBorra!= null) {
				session.delete(liBorra);
			}
			//se filtran libros 
			p.mostrarLibros(p.obtenerListaFiltrada(session, "Web"));
			transaccion.commit();
		}catch(HibernateException e){
			System.out.println(e.getMessage());
			transaccion.rollback();
		}finally {
			if(session != null) {
					session.close();
			}
		}
	}

	public List<Libro> obtenerListaLibros(Session sessionHiber){
		List<Libro> lista  = null;
		Query  consulta = sessionHiber.createQuery("FROM Libro libro");
		 lista =  consulta.list();
		 return lista;
	}
	public Libro mostrarSoloLibro(Session sesHib,String id) {
		Libro book = (Libro) sesHib.get(Libro.class, id);
		return book;
	}
	
	public void mostrarLibros(List<Libro> libros) {
		for(Libro l : libros) {
			System.out.print(l.getIsbn()+"\t");
			System.out.print(l.getTitulo()+"\t");
			System.out.println(l.getCategoria());
		}
	}
	
	public List<Libro> obtenerListaFiltrada(Session sesHib, String categoria){
		List<Libro> bookSearch = null;
		Query query = sesHib.createQuery("FROM Libro libros where libros.categoria =: categoria");
		query.setString("categoria", categoria);
		bookSearch = query.list();
		return bookSearch;
	}
}
